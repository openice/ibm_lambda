package lambda;
/*lambda关于线程*/
	
	   
	public class Java8Tester1{
	    public static void main(String args[]) {
	        int num = 1;
	        Converter<Integer, String> s = (param) -> System.out.println(String.valueOf(param + num));
	        s.convert(2);  // 输出结果为 3
	      //Before Java 8:
	        new Thread(new Runnable() {
	            @Override
	            public void run() {
	                System.out.println("Before Java8 ");
	            }
	        }).start();

	        //Java 8 way:
	        new Thread( () -> System.out.println("In Java8!") ).start();
	             
	    }
	 
	    public interface Converter<T1, T2> {
	        void convert(int i);
	    }
	}
