package lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Java8Tester3 {
	public static void main( String args[]){
		  List languages = Arrays.asList("Java", "Scala", "C++", "Haskell", "Lisp");
		  System.out.println("Languages which starts with J :");
		  filer1(languages,  (str)->(((String) str).contains("a")));
		/*  filter(languages, (str)->((String) str).startsWith("J"));

		  System.out.println("Languages which ends with a ");
		  filter(languages, (str)->((String) str).endsWith("a"));

		  System.out.println("Print all languages :");
		  filter(languages, (str)->true);

		   System.out.println("Print no language : ");
		   filter(languages, (str)->false);

		   System.out.println("Print language whose length greater than 4:");
		   filter(languages, (str)->((String) str).length() > 4);*/
		  Predicate<String> startsWithJ = (n) -> n.startsWith("J");
		  Predicate<String> fourLetterLong = (n) -> n.length() == 4;
		    
		  languages.stream()
		       .filter(startsWithJ.and(fourLetterLong))
		       .forEach((n) -> System.out.print("\nName, which starts with"
		            + "'J' and four letter long is : " + n));
		}

		 public static void filer1(List languages, Predicate condition) {
			 languages.stream().filter((language)->condition.test(language)).forEach((language)->System.out.println(language+""));
		
	}

		public static void filter(List<String> names, Predicate condition) {
		    for(String name: names)  {
		       if(condition.test(name)) {
		          System.out.println(name + " ");
		       }
		    }
		  }

	/*	Output:
		Languages which starts with J :
		Java
		Languages which ends with a
		Java
		Scala
		Print all languages :
		Java
		Scala
		C++
		Haskell
		Lisp
		Print no language :
		Print language whose length greater than 4:
		Scala
		Haskell*/

		//Even better
		/* public static void filter1(List names, Predicate condition) {
		    names.stream().filter((name) -> (condition.test(name)))
		        .forEach((name) -> {System.out.println(name + " ");
		    });
		 }*/
		 
}
