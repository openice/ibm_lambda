package lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
/*复杂的结合Predicate 使用*/
public class Java8Tester4 {
	public static void main(String[] args) {
		// We can even combine Predicate using and(), or() And xor() logical functions
		 // for example to find names, which starts with J and four letters long, you
		 // can pass combination of two Predicate
		List<String> names = Arrays.asList("Java", "Scala", "C++", "Haskell", "Lisp");
		 Predicate<String> startsWithJ = (n) -> n.startsWith("J");
		 Predicate<String> fourLetterLong = (n) -> n.length() == 4;
		 
		 names.stream().filter(startsWithJ.and(fourLetterLong)).forEach((n)->System.out.println("\nName, which starts with"
		            + "'J' and four letter long is : " + n));
	}
}
