package lambda;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Java8Tester6 {
	public static void main(String[] args) {
		List<String>  strList = Arrays.asList("abc","bcd","defg","jk");
		// Create a List with String more than 2 characters
		List<String> filtered = null;
		//filtered = strList.stream().filter(x -> x.length()> 2).collect(Collectors.toList());
		filtered = strList.stream().filter(n->n.length()>3).collect(Collectors.toList());                                   
		System.out.printf("Original List : %s, filtered list : %s %n", 
		                  strList, filtered);

		/*Output :
		Original List : [abc, , bcd, , defg, jk], filtered list : [abc, bcd, defg]*/
	}
}
