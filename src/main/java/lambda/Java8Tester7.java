package lambda;

import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

public class Java8Tester7 {
	public static void main(String[] args) {
		List<String> G7 = Arrays.asList("USA", "Japan", "France", "Germany", 
                "Italy", "U.K.","Canada");
		String collect = G7.stream().map(n->n.toUpperCase()).collect(Collectors.joining(","));
		System.out.println(collect+"lllllllllllllltest");

	}
	@Test
	public  void getfilter(){
		  List<String> languages = Arrays.asList("Java", "Scala", "C++", "Haskell", "Lisp");

		  System.out.println("Languages which starts with J :");
		  filter1(languages, (str)->((String) str).startsWith("J"));
		  System.out.println("===========================");
		  languages.stream().filter(n->n.startsWith("J")).collect(Collectors.toList()).forEach(System.out::printf);
		  System.out.println("Languages which ends with a ");
		  filter1(languages, (str)->((String) str).endsWith("a"));

		  System.out.println("Print all languages :");
		  filter1(languages, (str)->true);

		   System.out.println("Print no language : ");
		   filter1(languages, (str)->false);

		   System.out.println("Print language whose length greater than 4:");
		   filter1(languages, (str)->((String) str).length() > 4);
		}
	//Even better
	 public static void filter1(List<String> names, Predicate condition) {
	    names.stream().filter((name) -> (condition.test(name)))
	        .forEach((name) -> {System.out.println(name + " ");
	    });
	 }

		 public static void filter(List<String> names, Predicate condition) {
		    for(String name: names)  {
		       if(condition.test(name)) {
		          System.out.println(name + " ");
		       }
		    }
		  }

	
//	使用Lambda实现Map 和 Reduce
//	最流行的函数编程概念是map，它允许你改变你的对象，在这个案例中
//	我们将costBeforeTeax集合中每个元素改变了增加一定的数值，
//	我们将Lambda表达式 x -> x*x传送map()方法，这将应用到stream中所有元素
//	然后我们使用 forEach() 打印出这个集合的元素.
	@Test
	public void getByMap() {
		List<Integer> costBeforeTax = Arrays.asList(100, 200, 300, 400, 500);
		costBeforeTax.stream().map(n->n+1).forEach(System.out::println);
	}
	
	
//	reduce() 是将集合中所有值结合进一个，Reduce类似SQL语句中的sum(), avg() 或count() 
	@Test
	public void getByRedeuce() {
		List<Integer> costBeforeTax = Arrays.asList(100, 200, 300, 400, 500);
		int integer = costBeforeTax.stream().map(n->n+1).reduce((sum,n)->sum+n).get();
		System.out.println(integer);
	}
	
	
	/*通过filtering 创建一个字符串String的集合*/
	@Test
	public  void  getListByFilter() {
		List<String>  strList = Arrays.asList("abc","bcd","defg","jk");
		List<String> collect = strList.stream().filter(n->n.length()>2).collect(Collectors.toList());
		System.out.println(collect);
		
	}
	
	
	/*通过复制不同的值创建一个子列表
	使用Stream的distinct()方法过滤集合中重复元素。*/
	@Test
	public void  testByDistinct() {
		List<Integer> numbers = Arrays.asList(9, 10, 3, 4, 7, 3, 4);
		List<Integer> collect = numbers.stream().map(n->n*n).distinct().collect(Collectors.toList());
		System.out.printf("number is : %s, collect is :%s %n",numbers,collect);
	}
	
	
	/*	计算List中的元素的最大值，最小值，总和及平均值*/
	@Test
	public void getTheValueOfTheList() {
		List<Integer> primes = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29);
		IntSummaryStatistics stats = primes.stream().mapToInt((x) -> x)
                .summaryStatistics();
		System.out.println("Highest prime number in List : " + stats.getMax());
		System.out.println("Lowest prime number in List : " + stats.getMin());
		System.out.println("Sum of all prime numbers : " + stats.getSum());
		System.out.println("Average of all prime numbers : " + stats.getAverage());
	}
}
