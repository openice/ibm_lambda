package lambda;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*使用Lambda实现Map 和 Reduce
最流行的函数编程概念是map，它允许你改变你的对象，
在这个案例中，我们将costBeforeTeax集合中每个元素改变了增加一定的数值
，我们将Lambda表达式 x -> x*x传送map()方法，这将应用到stream中所有元素。
然后我们使用 forEach() 打印出这个集合的元素.*/
public class JavaTest5 {
	public static void main(String[] args) {
		// With Lambda expression:by map
		get();
		//bymap and reduce
		//getByReduce();
		

	}

	private static void getByReduce() {
		// Applying 12% VAT on each purchase
		// Old way:
		/*List costBeforeTax = Arrays.asList(100, 200, 300, 400, 500);
		double total = 0;
		for (Integer cost : costBeforeTax) {
		 double price = cost + .12*cost;
		 total = total + price;
		 
		}
		System.out.println("Total : " + total);
*/
		// New way:
		List<Integer> costBeforeTax = Arrays.asList(100, 200, 300, 400, 500);
		/*double bill = costBeforeTax.stream().map((cost) -> cost + .12*cost)
		                                    .reduce((sum, cost) -> sum + cost)
		                                    .get();
		System.out.println("Total : " + bill);	*/	
		Double double1 = costBeforeTax.stream().map(cost->cost+0.12*100).reduce((sum,cost)->sum + cost).get();
		System.out.println(double1);
		}

	private static void get() {
		List<Integer> costBeforeTax = Arrays.asList(100, 200, 300, 400, 500);
		/*costBeforeTax.stream().map((cost) -> cost + .12*cost)
		                      .forEach(System.out::println);*/	
		//costBeforeTax.stream().map(n->n+1).forEach(System.out::println);
		List<Integer> collect = costBeforeTax.stream().map(n->n+1).collect(Collectors.toList());
		List<Integer> collect2 = costBeforeTax.stream().filter(n->n>=300).map(n->n+2).collect(Collectors.toList());
		System.out.println(collect);
		System.out.println(collect2);
	}
}
